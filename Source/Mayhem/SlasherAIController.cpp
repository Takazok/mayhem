// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "SlasherAIController.h"
#include "Slasher.h"

void ASlasherAIController::BeginPlay() {
	Super::BeginPlay();
	currState = Start;
	AttackRange = 150.0f;
	player = UGameplayStatics::GetPlayerPawn(this, 0);
}

void ASlasherAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (currState != Dead && player != nullptr && GetPawn() != nullptr) {
		if (currState == Start) {
			if (player != nullptr) {
				MoveToActor(player);
				currState = Chase;
			}
		}
		else if (currState == Attack) {
			APawn* slasher = this->GetPawn();
			if (slasher != nullptr && player != nullptr) {
				if (FVector::Dist(slasher->GetActorLocation(), player->GetActorLocation()) > AttackRange) {
					MoveToActor(player);
					Cast<ASlasher>(slasher)->StopAttack();
				}
			}
		}
		if (Cast<ASlasher>(GetPawn())->getHealth() <= 0) currState = Dead;
	}
}

void ASlasherAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	APawn* dwarf = this->GetPawn();
	if (Result.IsSuccess() && dwarf != nullptr) {
		currState = Attack;
		Cast<ASlasher>(dwarf)->StartAttack();
	}
}
