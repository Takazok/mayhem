// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Sound/SoundCue.h"
#include "GameFramework/Character.h"
#include "AI_Character.generated.h"

UCLASS()
class MAYHEM_API AAI_Character : public ACharacter
{
	GENERATED_BODY()

	
protected:
	// sounds 
	UPROPERTY(Transient)
		class UAudioComponent* SoundAC;
	UPROPERTY(Transient)
		class UAudioComponent* WalkSoundAC;
	UPROPERTY(EditAnywhere, Category = Sounds)
		class USoundCue* DeathSound;
	UPROPERTY(EditAnywhere, Category = Sounds)
		class USoundCue* WalkSound;
	UPROPERTY(EditAnywhere, Category = Sounds)
		class USoundCue* AttackSound;

	void PlaySound(USoundCue* sound);

	// stats
	UPROPERTY(EditAnywhere)
		float Damage = 10.0f;
	UPROPERTY(EditAnywhere)
		float Health = 50.0f;
	UPROPERTY(EditAnywhere)
		float DeathTimer = 0.15f;
	UPROPERTY(EditAnywhere)
		float AttackRange = 35.0f;
	UPROPERTY(EditAnywhere)
		float AggroRange = 3000.0f;

	// desires
	UPROPERTY(EditAnywhere)
		float mMovementDesire = 0.5f;
	UPROPERTY(EditAnywhere)
		float spreadDesire = 5.0f;
	UPROPERTY(EditAnywhere)
		float spreadDistance = 500.0f;
	UPROPERTY(EditAnywhere)
		float survivabilityDesire = 1.0f;
	UPROPERTY(EditAnywhere)
		float killDesire = 1.0f;


	UPROPERTY(EditAnywhere)
		float thinkDelay = 0.5f;
	UPROPERTY(EditAnywhere)
		bool isMelee = true;

	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> AttackProjectileClass;

	// self destruction if it's a Fatty
	UPROPERTY(EditAnywhere)
		TSubclassOf<class AActor> destructionClass = nullptr;
public:
	// sound
	void PlayAttackSound();
	void PlayWalkSound();
	void PlayDeathSound();
	void StopSound();

	// Sets default values for this character's properties
	AAI_Character();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;


	void DoRangedAttack();
	void StopAttack() {};

	float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;

	void DestroyMe();
	void DamagePlayer();
	int getHealth() const { return Health; }
	FTimerHandle attackTimer;

	// setters
	void SetHealth(float x ) { Health = x;  }

	// getters
	float GetAttackRange() const { return AttackRange;  }
	float GetThinkDelay() const { return thinkDelay; }
	float GetAggroRange() const { return AggroRange; }
	float GetAttackDamage() const { return Damage; }
	float GetHealth() const { return Health; }


	// desires
	float GetMovementDesire() const { return mMovementDesire;  }
	float GetSpreadDesire() const { return spreadDesire; }
	float GetSpreadDistance() const { return spreadDistance; }
	float GetSurvivabilityDesire() const { return survivabilityDesire; }
	float GetKillDesire() const { return killDesire; }
	bool GetIsMelee() const { return isMelee; }

};
