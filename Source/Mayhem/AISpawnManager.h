// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AI_Character.h"
#include "GameFramework/Actor.h"
#include "AISpawnManager.generated.h"

UCLASS()
class MAYHEM_API AAISpawnManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAISpawnManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	UPROPERTY(EditAnywhere, Category = Spawn)
		TArray<class ATargetPoint*> SpawnPoints;
	UPROPERTY(EditAnywhere, Category = Spawn)
		TArray<TSubclassOf<AAI_Character>> AICharacterClasses;
	UPROPERTY(EditAnywhere, Category = Spawn)
		unsigned MaxEnemies = 9;
	UPROPERTY(EditAnywhere, Category = Spawn)
		float MinSpawnTime = 1.0f;
	UPROPERTY(EditAnywhere, Category = Spawn)
		float SpawnTimeDelta = -0.125f;
	UPROPERTY(EditAnywhere, Category = Spawn)
		float SpawnTime = 3.0f;

	UPROPERTY(EditAnywhere, Category = Spawn)
		float CurrentHealthDelta = 0.0f;
	UPROPERTY(EditAnywhere, Category = Spawn)
		float HealthDelta = .025f;
	UPROPERTY(EditAnywhere, Category = Spawn)
		float MaxHealthDelta = 0.5f;

	void BuffEnemy(AAI_Character*);
	void OnSpawnTimer();
	unsigned GetNumberOfEnemies();
	void ChangeSpawnTime();
protected:
	FTimerHandle SpawnTimer;
};
