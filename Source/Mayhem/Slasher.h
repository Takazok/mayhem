// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Slasher.generated.h"

UCLASS()
class MAYHEM_API ASlasher : public ACharacter
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere)
		float Health = 20.0f;
	UPROPERTY(EditAnywhere)
		float SlasherDamage = 10.0f;

public:
	// Sets default values for this character's properties
	ASlasher();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void StartAttack();
	void StopAttack();

	float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;

	void DestroyMe();
	void DamagePlayer();
	int getHealth() { return Health; }
	FTimerHandle attackTimer;
};
