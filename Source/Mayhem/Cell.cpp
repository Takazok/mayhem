// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "Cell.h"

Cell::Cell(float x, float y, FVector origin) {
	mCellWidth = x;
	mCellHeight = y;
	mPosition = origin;
}

Cell::Cell() {
	mCellWidth = 0;
	mCellHeight = 0;
	mPosition = FVector(0, 0, 0);
	mThreat = 100000;
}

Cell::~Cell()
{
}
