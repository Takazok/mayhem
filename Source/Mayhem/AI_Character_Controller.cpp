// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "AI_Character_Controller.h"
#include "MayhemCharacter.h"
#include "AI_Character.h"
#include "Engine.h"
#include "Math.h"
#include "ThreatMap.h"


void AAI_Character_Controller::BeginPlay() {
	Super::BeginPlay();
	currState = Start;
	//AttackRange = 5000.0f;
	player = UGameplayStatics::GetPlayerPawn(this, 0);
}

void AAI_Character_Controller::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// dead? do nothing
	if (IsDeadState() || Cast<AMayhemCharacter>
		(UGameplayStatics::GetPlayerPawn(this, 0))->isDead()) {
		return;
	}

	// still waiting to think? do nothing
	if (!ShouldThink(DeltaSeconds)) {
		return;
	}
	
	

	// move -- don't need to see player to know where to move
	if (this->GetMovementDesire() > 0.001)
		MoveToOptimalLocation();

	// attack (TODO)
	StartAttack();

	// player not in sight? do nothing
	if (!SearchForPlayer()) {
		return;
	}

}

void AAI_Character_Controller::MoveToOptimalLocation() {
	for (TActorIterator<AThreatMap> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
		AThreatMap* ai = *ActorItr;
		FVector loc = ai->GetOptimalLocation(this);
		/*
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
			FString::Printf(TEXT("Moving to optimal location: %f, %f, %f"), 
			loc.X, loc.Y, loc.Z));
		*/
		MoveToLocation(loc, 15, false);
	}
}

void AAI_Character_Controller::StartAttack() {
	float distanceToPlayer = GetDistanceToPlayer();
	float atkRange = GetAttackRange();
	if (distanceToPlayer <= atkRange) {
		DoAttack();
	}
}

void AAI_Character_Controller::DoAttack() {
	//this->StopMovement();
	auto* player = Cast<AMayhemCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	auto* ai = Cast<AAI_Character>(this->GetPawn());
	FRotator rot = (player->GetActorLocation() - ai->GetActorLocation()).Rotation();
	ai->SetActorRotation(rot);

	if (player->isDead())
		return;

	if (GetIsMelee()) {
		PlayAttackSound();
		player->TakeDamage(this->GetAttackDamage(), FDamageEvent(),
			this, ai);
	} else {
		ai->DoRangedAttack();
	}
}

void AAI_Character_Controller::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	return;
	APawn* dwarf = this->GetPawn();
	if (Result.IsSuccess() && dwarf != nullptr) {
		currState = Attack;
		//Cast<AAI_Character>(dwarf)->StartAttack();
	}
}

// there must be a pause between each cycle of the AI's state machine
// this simply returns true if the pause exists at the time that the function is called
bool AAI_Character_Controller::ShouldThink(float deltaTime) {
	if (thinkDelayCount < GetThinkDelay()) {
		thinkDelayCount += deltaTime;
		return false;
	}
	else {
		thinkDelayCount = 0;
		return true;
	}
}

// well the enemies don't die so this really doesn't do much
bool AAI_Character_Controller::IsDeadState() {
	auto AIChar = Cast<AAI_Character>(this->GetPawn());
	if (AIChar == nullptr) {
		canThink = false;
	} else {
		canThink = AIChar->getHealth() > 0;
	}
	return !canThink;
}

// true if player is found
// player is found if:
//  1. In range -- player must be w/i a certain distance
//	2. Line of sight -- view not obstructed
//  3. FOV -- player must be w/i ai's field of vision
bool AAI_Character_Controller::SearchForPlayer() {
	if (IsPlayerInRange(VisionRange)) {
		if (IsPlayerInFieldOfView(FieldOfView)) {
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
				//FString::Printf(TEXT("PLAYER IN FOV")));

			if (IsPlayerInLineOfSight()) {
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
				//	FString::Printf(TEXT("PLAYER IN LINE OF SIGHT")));
				return true;
			}
		}
	}
	return false;
}

bool AAI_Character_Controller::IsPlayerInRange(float distance) {
	FVector playerPos = UGameplayStatics::GetPlayerPawn(this, 0)->GetTransform().GetLocation();
	FVector aiPos = this->GetPawn()->GetTransform().GetLocation();
	return FVector::Dist(playerPos, aiPos) <= distance;
}

bool AAI_Character_Controller::IsPlayerInLineOfSight() {
	FVector rayOrigin = this->GetPawn()->GetTransform().GetLocation();
	auto player = UGameplayStatics::GetPlayerPawn(this, 0);

	return LineOfSightTo(player, rayOrigin, true);
}


bool AAI_Character_Controller::IsPlayerInFieldOfView(float FOV) {
	FVector aiPos = this->GetPawn()->GetTransform().GetLocation();
	FVector playerPos = UGameplayStatics::GetPlayerPawn(this, 0)->GetTransform().GetLocation();
	FVector vecToPlayer = (playerPos - aiPos);

	FVector2D vecToPlayer2D(vecToPlayer.X, vecToPlayer.Y);
	vecToPlayer2D.Normalize();

	FVector forward = this->GetPawn()->GetActorForwardVector();
	FVector2D forward2D(forward.X, forward.Y);
	forward2D.Normalize();

	float angle = 360.0 / 2 / 3.14152 * acos(FVector2D::DotProduct(vecToPlayer2D, forward2D));

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
	//	FString::Printf(TEXT("PLAYER ANGLE: %f"), angle));
	return (abs((long) angle) <= FOV / 2);
}

bool AAI_Character_Controller::LineOfSight(FVector origin) {
	auto player = UGameplayStatics::GetPlayerPawn(this, 0);

	return LineOfSightTo(player, origin, true);
}

float AAI_Character_Controller::GetDistanceToPlayer() {
	FVector playerLoc = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
	FVector aiLoc = this->GetPawn()->GetActorLocation();
	return FVector::Dist(playerLoc, aiLoc);
}