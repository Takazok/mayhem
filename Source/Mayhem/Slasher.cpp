// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "Slasher.h"
#include "SlasherAIController.h"
#include "Engine.h"


// Sets default values
ASlasher::ASlasher()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AIControllerClass = ASlasherAIController::StaticClass();

}

// Called when the game starts or when spawned
void ASlasher::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlasher::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ASlasher::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void ASlasher::StartAttack()
{
	//float d = PlayAnimMontage(AttackAnim);
	GetWorldTimerManager().SetTimer
	(
		attackTimer,
		this,
		&ASlasher::DamagePlayer,
		1.0f,
		true
	);
}

void ASlasher::StopAttack()
{
	/*StopAnimMontage(AttackAnim);*/
	GetWorldTimerManager().ClearTimer(attackTimer);
}

float ASlasher::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
		EventInstigator, DamageCauser);

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, TEXT("Zombie dying"));
	if (ActualDamage > 0.0f)
	{
		Health -= ActualDamage;
		if (Health <= 0.0f)
		{
			// We're dead, don't allow further damage
			bCanBeDamaged = false;
			StopAttack();
			float deathDurration = 0.5f; //animation time - delay

			FTimerHandle DwarfDeathTimer;

			GetWorldTimerManager().SetTimer
			(
				DwarfDeathTimer,
				this,
				&ASlasher::DestroyMe,
				deathDurration,
				false,
				deathDurration
			);
			this->GetController()->UnPossess();
		}
	}
	return ActualDamage;
}

void ASlasher::DestroyMe() {
	this->Destroy();
}

void ASlasher::DamagePlayer() {
	APawn* player = UGameplayStatics::GetPlayerPawn(this, 0);
	player->TakeDamage(SlasherDamage, FDamageEvent(), GetInstigatorController(), this);
}