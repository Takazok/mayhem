// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "AI_Character.h"
#include "AISpawnManager.h"
#include "Engine/TargetPoint.h"
#include "EngineUtils.h"

// Sets default values
AAISpawnManager::AAISpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAISpawnManager::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(SpawnTimer, this,
		&AAISpawnManager::OnSpawnTimer, SpawnTime, true);
}

// Called every frame
void AAISpawnManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AAISpawnManager::OnSpawnTimer() {
	ChangeSpawnTime();
	int num = SpawnPoints.Num();
	bool reachedEnemyLimit = GetNumberOfEnemies() >= MaxEnemies;
	bool hasAIClasses = AICharacterClasses.Num() > 0;
	if (num == 0 || reachedEnemyLimit || !hasAIClasses){
		return;
	}
	int index = FMath::RandRange(0, num - 1);
	auto spawnPoint = SpawnPoints[index];
	FVector pos = spawnPoint->GetActorLocation();
	FRotator rot = spawnPoint->GetActorRotation();

	index = FMath::RandRange(0, AICharacterClasses.Num()-1);
	auto aiClass = AICharacterClasses[index];
	AAI_Character* Char = GetWorld()->SpawnActor<AAI_Character>
		(aiClass, pos, rot);
	if (Char) {
		// Spawn the AI controller for the character
		Char->SpawnDefaultController();
		BuffEnemy(Char);
	}
}

void AAISpawnManager::BuffEnemy(AAI_Character* ai) {

	ai->SetHealth(ai->GetHealth() * (1 + CurrentHealthDelta));
	CurrentHealthDelta += HealthDelta;
	if (CurrentHealthDelta > MaxHealthDelta) {
		CurrentHealthDelta = MaxHealthDelta;
	}
}

unsigned AAISpawnManager::GetNumberOfEnemies() {
	unsigned numEnemies = 0;
	for (TActorIterator<AAI_Character> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		numEnemies++;
	}
	return numEnemies;
}

void AAISpawnManager::ChangeSpawnTime() {
	SpawnTime += SpawnTimeDelta;
	if (SpawnTime < MinSpawnTime) {
		SpawnTime = MinSpawnTime;
	}
	else {
		// reset the timer
		GetWorldTimerManager().ClearTimer(SpawnTimer);
		GetWorldTimerManager().SetTimer(SpawnTimer, this,
			&AAISpawnManager::OnSpawnTimer, SpawnTime, true);
	}
}