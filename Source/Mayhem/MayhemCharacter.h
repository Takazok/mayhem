// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "MayhemCharacter.generated.h"

class UInputComponent;


UCLASS(config=Game)
class AMayhemCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;	
	
	UPROPERTY(EditAnywhere, Category = Character)
		uint16 WalkSpeed = 350;

	/** The speed at which the character will be sprinting*/
	UPROPERTY(EditAnywhere, Category = Character)
		uint16 SprintSpeed = 650;

	UPROPERTY(EditAnywhere, Category = Stats)
		float MaxHealth = 100.0f;
	UPROPERTY(EditAnywhere, Category = Stats)
		float Health = 100.0f;
	UPROPERTY(EditAnywhere, Category = Stats)
		float HealthRegen = 3.0f;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float MaxEnergy = 20;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		float Energy = 20;
	UPROPERTY(EditAnywhere, Category = Stats)
		float MaxEnergyRegen = 4;
	UPROPERTY(EditAnywhere, Category = Stats)
		float EnergyRegen = 4.0f;
	UPROPERTY(EditAnywhere, Category = Stats)
		float SprintEnergyCost = 5;

	class AWeapon* mWeapon;

public:
	AMayhemCharacter();

	bool ReduceEnergyFromWeapon(float);

	virtual void BeginPlay();
	virtual void Tick(float) override;
	float FlashlightBrightness = 1;
	FTimerHandle FlashlightRepairTimer;
	void SetFlashlightIntensity();
	void RepairFlashlight();
	void DoEMP();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
		float WeaponRange;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
		float WeaponDamage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
		float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float EnergyBar = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float HealthBar = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadOnly, Category = Stats)
		int ZombieKills = 0;

	UPROPERTY(EditAnywhere, Category = Sound)
		class USoundCue* FireLoopSound;
	UPROPERTY(EditAnywhere, Category = Sound)
		class USoundCue* FireFinishSound;
	UPROPERTY(Transient)
		class UAudioComponent* FireAC;

	UPROPERTY(EditAnywhere, Category = Effects)
		class UParticleSystem* MuzzleFX;
	UPROPERTY(Transient)
		class UParticleSystemComponent* MuzzlePSC;

	UAnimInstance* FireAnimInstance;
	void StartFireAnimation();
	void StopFireAnimation();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;

	void IncreaseZombieKills() { ZombieKills += 1; }

protected:
	
	/** Fires a projectile. */
	void OnStartFire();
	void OnStopFire() { OnStopFire(true); }
	void OnStopFire(bool triggerReleased=true);

	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	void onJump();

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);


	// weapon
	UPROPERTY(EditAnywhere, Category = Weapon)
		TSubclassOf<class AWeapon> WeaponClass;

	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override; 


	/** Enables Sprinting for the character*/
	void EnableSprint();

	/** Disables Sprinting again */
	void DisableSprint();
	// End of APawn interface

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;

	void OnDie();

	AWeapon* GetWeapon() { return mWeapon;  }
	bool isDead();
};

