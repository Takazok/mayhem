// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include <vector>
#include "Cell.h"
#include "AI_Character.h"
#include "AI_Character_Controller.h"
#include "GameFramework/Pawn.h"
#include "ThreatMap.generated.h"


UCLASS()
class MAYHEM_API AThreatMap : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AThreatMap();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// functions
	void CreateCells(unsigned resolution, FVector2D, FVector2D);
	//void UpdateThreat();
	//FVector GetMoveLocationForEnemy(APawn&);
	FVector GetOptimalLocation(AAI_Character_Controller*);

protected:
	UPROPERTY(EditAnywhere) 
		float mBoundsOffset = 2000.0f;
	UPROPERTY(EditAnywhere) 
		unsigned mResolution = 300;
	UPROPERTY(EditAnywhere)
		float mUpdateDelay = 0.5f;

private:
	float mCellWidth = 1;

	bool UpdateTime(float);
	float mUpdateDelayCount = 0;

	TArray<TArray<Cell>> mCells;
	//void UpdateCellThreat(Cell&, APawn&, std::vector<APawn&>);
	void GetThreatMapBounds(FVector2D&, FVector2D&);
	float GetTerrainHeight(float x, float y);


	// heuristic functions
	//   would be nice to have them in a separate class..

	// ENEMY PROXIMITY HEURISTICS =============
	float SumOfEnemiesPositionHeuristic(const Cell&);
	float SumOfEnemiesPositionHeuristic(const Cell&, const AAI_Character*);
	
	// enemies are safer when they are closer together	
	float EnemiesProximitySafetyHeuristic(const Cell&, const AAI_Character*); 
	
	// enemies are not safe when they are TOO close together; they should spread apart slightly
	float EnemiesProximityDangerHeuristic(const Cell&, const AAI_Character*, const AAI_Character* x = nullptr);

	// ENEMY KILL HEURISTICS ==================
	float EnemyKillHeuristic(const Cell&, const AAI_Character*);
	float EnemyKillMeleeHeuristic(const Cell&, const AAI_Character*);
	float EnemyKillRangedHeuristic(const Cell&, const AAI_Character*);

	// PLAYER HEURISTICS ======================
	float PlayerProximityDangerHeuristic(const Cell&, const AAI_Character*);
	float PlayerFieldOfViewDangerHeuristic(const Cell&, const AAI_Character*);
	float PlayerLineOfSightDangerHeuristic(const Cell&, const AAI_Character*);

	// MOVEMENT HEURISTICS ====================
	float MovementHeuristic(const Cell&, const AAI_Character*, float threat);

	// HELPER FOR HEURISTICS ==================
	float DistanceBetweenCellAndActor(const Cell & cell, const AActor * actor);
	float DistanceBetweenActorAndActor(const AActor*, const AActor*);

	// takes two numbers and normalizes it to some value [0, 1). The first parameter is
	// the main value, the 2nd parameter determines how quickly the first parameter approaches
	// 1. Think of this function like half of a sigmoid function
	float Normalize1(float, float growth = 2);

	// gotta map negative values to positive ones
	float Sigmoid(float);

	// like square/cubic root.. returns value ^ (1/rootAmount)
	float Root(float value, float rootAmount);

	bool CanMoveToLocation(FVector, AAI_Character*);
	void PauseMoveToLocation(AAI_Character*);


	// may be used for optimization later
	// rather than having the AI call the heuristics when they need it (which makes a bunch of superfluous function calls)
	// just have the cells/threatmap do it and then let the AI get data from it; this would avoid extra calculations
	void UpdateAllCellThreats();
};
