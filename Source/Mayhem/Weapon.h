// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Sound/SoundCue.h"
#include "MayhemCharacter.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS()
class MAYHEM_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	//WeaponMesh GetWeaponMesh();
	USkeletalMeshComponent* GetWeaponMesh();

	void OnStartFire();
	void OnStopFire(bool triggerReleased=true);

	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	AMayhemCharacter* GetOwner();
	void SetOwner(AMayhemCharacter*);

	void WeaponTrace();
	void TryWeaponFire();
	float GetEnergyCost() { return EnergyCost;  }
	bool isFiring = false;

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
		USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* FireLoopSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* FireFinishSound;
	UPROPERTY(Transient)
		class UAudioComponent* FireAC;

	UPROPERTY(EditDefaultsOnly, Category = Effect)
		UParticleSystem* MuzzleFX;
	UPROPERTY(transient) UParticleSystemComponent* MuzzleFXComponent;

	AMayhemCharacter* MyOwner;

	UPROPERTY(EditAnywhere, Category = Weapon)
		float FireRate;
	UPROPERTY(EditAnywhere, Category = Weapon)
		float WeaponRange;
	UPROPERTY(EditDefaultsOnly, Category = Weapon)
		UParticleSystem* ImpactParticle;

	UPROPERTY(EditAnywhere, Category = Damage)
		float Damage = 10.0f;
	UPROPERTY(EditAnywhere, Category = Stats)
		float EnergyCost = 1.0f;

	FTimerHandle WeaponTimer;
};