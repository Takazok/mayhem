// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Mayhem.h"
#include "MayhemCharacter.h"
#include "MayhemProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "Engine.h"
#include "AI_Character.h"
#include "Slasher.h"
#include "Weapon.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AMayhemCharacter

AMayhemCharacter::AMayhemCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

		// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void AMayhemCharacter::Tick(float deltaTime) {
	
	// regenerate stuff only if you're alive
	if (Health > 0) { 
		// regenerate health
		Health += deltaTime * HealthRegen;
		if (Health > MaxHealth) {
			Health = MaxHealth;
		}

		// regenerate energy
		if (Energy < 0) { // in case you go below it 
			if (Energy < -15) {
				// this must be from an EMP
				DoEMP();
			}
			Energy = 0;
		}
		Energy += deltaTime * EnergyRegen;
		if (Energy > MaxEnergy) {
			Energy = MaxEnergy;
		}
	}

	HealthBar = Health / MaxHealth; // for GUI
	EnergyBar = Energy / MaxEnergy;

}


void AMayhemCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	if (WeaponClass) {
		UWorld* World = GetWorld();
		if (World) {
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Need to set rotation like this because otherwise gun points down
			FRotator Rotation(0.0f, 0.0f, 0.0f);
			// Spawn the Weapon
			mWeapon = World->SpawnActor<AWeapon>(WeaponClass, FVector::ZeroVector,
				Rotation, SpawnParams);
			if (mWeapon) {
				// This is attached to "WeaponPoint" which is defined in the skeleton
				mWeapon->AttachToComponent(Mesh1P,
					FAttachmentTransformRules::KeepRelativeTransform,
					TEXT("GripPoint"));
				//mWeapon->GetWeaponMesh()->SetupAttachment(Mesh1P,
					//TEXT("GripPoint"));
				mWeapon->SetOwner(this);
			}
		}
	}

	//WalkSpeed = 250;
	//SprintSpeed = 600;
	EnableSprint();
	DisableSprint();
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMayhemCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMayhemCharacter::onJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AMayhemCharacter::StopJumping);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMayhemCharacter::EnableSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMayhemCharacter::DisableSprint);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMayhemCharacter::TouchStarted);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMayhemCharacter::OnStartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AMayhemCharacter::OnStopFire);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMayhemCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMayhemCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMayhemCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMayhemCharacter::LookUpAtRate);
}

void AMayhemCharacter::onJump()
{
	if(!isDead()){
		this->ACharacter::Jump();
	}
}

void AMayhemCharacter::EnableSprint() 
{
	this->GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
	//	FString::Printf(TEXT("regen (%f) vs cost (%f)"), EnergyRegen, SprintEnergyCost));
	EnergyRegen -= SprintEnergyCost;
}

void AMayhemCharacter::DisableSprint()
{
	this->GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, TEXT("Walkin"));
	EnergyRegen += SprintEnergyCost;
}

void AMayhemCharacter::OnStartFire()
{
	if (mWeapon != nullptr && !isDead()) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("ON START FIRE"));
		if (mWeapon->GetEnergyCost() <= Energy) {
			mWeapon->OnStartFire();
		}
	}
	else {
		GEngine->AddOnScreenDebugMessage(-1, 5.f,
			FColor::Red, TEXT("ON START FIRE .. but null weapon"));
	}
	return;
	/*
	// try and fire a projectile
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("ON START FIRE"));
	if (!isDead()) {
		if (ProjectileClass != NULL)
		{
			UWorld* const World = GetWorld();
			if (World != NULL)
			{
				if (false) // bUsingMotionControllers)
				{
					const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
					const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
					World->SpawnActor<AMayhemProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
				}
				else
				{
					//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SETTING TIMER FOR WEAPON TRACE"));
					float delay = 0.01; // 1 / FireRate
					FTimerHandle theTimer;
					GetWorldTimerManager().SetTimer(theTimer, this, &AMayhemCharacter::WeaponTrace,
						delay, false);
					if (FireAC)
						FireAC = PlayWeaponSound(FireLoopSound);
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PAST WEAPON SOUND"));
					
					if (MuzzlePSC != nullptr && MuzzleFX != nullptr)
						MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, FP_Gun, TEXT("MuzzleFlashSocket"));

					//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PAST MUZZLE STUFF"));
				}
			}
		}

		// try and play the sound if specified
		if (FireSound != NULL)
		{
			if (FireSound != nullptr)
				UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}

		// try and play a firing animation if specified
		if (FireAnimation != NULL && FireAnimation != nullptr)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
			if (AnimInstance != NULL)
			{
				AnimInstance->Montage_Play(FireAnimation, 1.f);
			}
		}
	}
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("PAST ON START FIRE STUFF"));
	*/
}

void AMayhemCharacter::StartFireAnimation() {
	FireAnimInstance = Mesh1P->GetAnimInstance();
	FireAnimInstance->Montage_Play(FireAnimation, .2f);
}

void AMayhemCharacter::StopFireAnimation() {
	FireAnimInstance->Montage_Stop(0.1f);
}

UAudioComponent* AMayhemCharacter::PlayWeaponSound(USoundCue* Sound)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("STARTING SOUND STUFF"));
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
	}
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("PAST SOUND STUFF"));
	return AC;
}

void AMayhemCharacter::OnStopFire(bool triggerReleased) {
	if (mWeapon != nullptr) {
		mWeapon->OnStopFire(triggerReleased);
	}
	return;
	//GetWorldTimerManager().ClearTimer(WeaponTimer);
	
	if (FireFinishSound != nullptr) 
		PlayWeaponSound(FireFinishSound);
	
	if (MuzzlePSC != nullptr)
		MuzzlePSC->DeactivateSystem();
	
	if (FireAC != nullptr)
		FireAC->Stop();
}

void AMayhemCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AMayhemCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AMayhemCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMayhemCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

float AMayhemCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
		EventInstigator, DamageCauser);
	if (ActualDamage > 0.0f)
	{
		Health -= ActualDamage;
		if (Health <= 0.0f)
		{
			// We're dead, don't allow further damage
			bCanBeDamaged = false;
			//OnStopFire();
			//float deathDurration = PlayAnimMontage(DeathAnim);

			APlayerController* PC = Cast<APlayerController>(GetController());
			if (PC) {
				PC->SetCinematicMode(true, true, true);
			}

			FTimerHandle PlayerDeathTimer;
			GetWorldTimerManager().SetTimer(PlayerDeathTimer, this, &AMayhemCharacter::OnDie, 0.5f, false); //death duration based on animation - delay for 4th param
		}
	}
	return ActualDamage;
}

void AMayhemCharacter::OnDie() {
	Energy = 0;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, TEXT("My ass should be dead"));
	GetMesh()->Deactivate();
}

bool AMayhemCharacter::isDead() {
	return (Health <= 0.0f);
}

bool AMayhemCharacter::ReduceEnergyFromWeapon(float val) {
	// costs too much energy, so don't do fire the weapon
	if (val > Energy) {
		//OnStopFire(false);
		return false;
	}
	Energy -= val;
	return true;
}

void AMayhemCharacter::DoEMP() {
	// turn off flash light
	//GetWorldTimerManager().SetTimer(FlashlightRepairTimer, 
	//	this, &AMayhemCharacter::RepairFlashlight, 
	//	10.0f, true, 0);

	EnergyRegen *= 0.50;
	FlashlightBrightness = 0;
	//SetFlashlightIntensity();
}

void AMayhemCharacter::RepairFlashlight() {
	if (FlashlightBrightness >= 1) {
		FlashlightBrightness = 1;
		GetWorldTimerManager().ClearTimer(FlashlightRepairTimer);
	}
	else {
		FlashlightBrightness += 0.1f;
		SetFlashlightIntensity();
	}
}

void AMayhemCharacter::SetFlashlightIntensity() {
	TArray<AActor*> actors;
	UGameplayStatics::GetAllActorsWithTag(this,
		TEXT("FLASHLIGHT"), actors);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
		FString::Printf(TEXT("num actors w/ tag: %f"),
		actors.Num()));
	if (actors.Num() > 0) {
		auto spotlight = Cast<ASpotLight>(actors[0]);
		spotlight->SetBrightness(FlashlightBrightness);
		spotlight->SetEnabled(false);
	}
}