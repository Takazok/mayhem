// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/TriggerBox.h"
#include "CheckpointTriggerBox.generated.h"

/**
 * 
 */
UCLASS()
class MAYHEM_API ACheckpointTriggerBox : public ATriggerBox
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACheckpointTriggerBox();

protected:

private:

	UPROPERTY(EditAnywhere)
		class ACheckpointManager* Manager;
};
