// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "AI_Character.h"
#include "AI_Character_Controller.generated.h"

/**
 * 
 */
UCLASS()
class MAYHEM_API AAI_Character_Controller : public AAIController
{
	GENERATED_BODY()
	
	
public:
	void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result) override;

	float GetAttackRange() {
		return Cast<AAI_Character>(this->GetPawn())->GetAttackRange();
	}
	float GetMovementDesire() { 
		return Cast<AAI_Character>(this->GetPawn())->GetMovementDesire();
	}
	float GetSurvivabilityDesire() {
		return  Cast<AAI_Character>(this->GetPawn())->GetSurvivabilityDesire();
	}
	float GetThinkDelay() {
		return Cast<AAI_Character>(this->GetPawn())->GetThinkDelay();
	}
	float GetAttackDamage() {
		return Cast<AAI_Character>(this->GetPawn())->GetAttackDamage();
	}
	float GetKillDesire() {
		return Cast<AAI_Character>(this->GetPawn())->GetKillDesire();
	}
	bool GetIsMelee() {
		return Cast<AAI_Character>(this->GetPawn())->GetIsMelee();
	}

	void PlayDeathSound() { 
		return Cast<AAI_Character>(this->GetPawn())->PlayDeathSound(); }
	void PlayWalkSound() { 
		return Cast<AAI_Character>(this->GetPawn())->PlayWalkSound(); }
	void PlayAttackSound() { 
		return Cast<AAI_Character>(this->GetPawn())->PlayAttackSound(); }
	void StopSound() { 
		return Cast<AAI_Character>(this->GetPawn())->StopSound(); }


	bool LineOfSight(FVector);
private:
	enum state { Start, Chase, Attack, Dead };
	state currState;
	APawn* player;

	bool ShouldThink(float);
	bool IsDeadState();

	void MoveToOptimalLocation();

	bool SearchForPlayer();

	// attacking
	void StartAttack();
	void DoAttack();

	// helpers
	bool IsPlayerInRange(float);
	bool IsPlayerInLineOfSight();
	bool IsPlayerInFieldOfView(float);
	float GetDistanceToPlayer();
protected:
	

	UPROPERTY(EditAnywhere)
		float thinkDelayCount = 0.0f;

	UPROPERTY(EditAnywhere)
		bool canThink = true;
	
	UPROPERTY(EditAnywhere)
		float FieldOfView = 160.0f;
	
	UPROPERTY(EditAnywhere)
		float VisionRange = 5000.0f;

};
