// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Mayhem.h"

/**
 * 
 */
class MAYHEM_API Cell
{
public:
	Cell();
	Cell(float, float, FVector);
	~Cell();

	FVector GetPosition() const { return mPosition;  }

	void SetThreat(float threat) { mThreat = threat;  }
	float GetThreat() { return mThreat; }

private:
	float mCellWidth;
	float mCellHeight;

	FVector mPosition;

	// there are different factors involved in determing the threat
	//    eg: player fov, player position, enemies' positions, etc
	// however some of this knowledge (like player position) is 
	//    unknown to the enemy, so we have to exclude this knowledge
	// we exclude this knowledge by having different threat values
	//    threat values for when the player info is known, and not known, etc
	float mThreat = 0;
	float mThreatFromEnemies; // negative
	float mThreatFromPlayer; // positive, but only 
	
};

