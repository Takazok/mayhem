// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "Weapon.h"
#include "AI_Character.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine.h"

// Sets default values
AWeapon::AWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = WeaponMesh;
	FireRate = 0.1f;
	WeaponRange = 10000.0f;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

USkeletalMeshComponent* AWeapon::GetWeaponMesh() {
	return WeaponMesh;
}

UAudioComponent* AWeapon::PlayWeaponSound(USoundCue* Sound) {
	UAudioComponent* AC = NULL;
	if (Sound) {
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
	}
	return AC;
}

void AWeapon::OnStartFire() {
	isFiring = true;
	FireAC = PlayWeaponSound(FireLoopSound);

	// particles
	MuzzleFXComponent = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, WeaponMesh);
	MuzzleFXComponent->AttachToComponent(WeaponMesh,
		FAttachmentTransformRules::KeepRelativeTransform,
		TEXT("MuzzleFlashSocket"));
	
	// weapon firing ray trace
	GetWorldTimerManager().SetTimer(WeaponTimer, this, &AWeapon::TryWeaponFire,
		FireRate, true, 0);
}

void AWeapon::TryWeaponFire() {
	// energy cost
	if (!this->GetOwner()->ReduceEnergyFromWeapon(EnergyCost)) {
		// can't fire
		if (isFiring)
			OnStopFire(false);
	} else {
		// can fire.. continue firing (ie: just weapon trace) or begin firing again?
		if (!isFiring) {
			FireAC = PlayWeaponSound(FireLoopSound);

			// particles
			MuzzleFXComponent = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, WeaponMesh);
			MuzzleFXComponent->AttachToComponent(WeaponMesh,
				FAttachmentTransformRules::KeepRelativeTransform,
				TEXT("MuzzleFlashSocket"));
		}
		isFiring = true;
		WeaponTrace();
	}
}

void AWeapon::OnStopFire(bool triggerReleased) {
	if (FireAC != nullptr) {
		FireAC->Stop();
	}
	PlayWeaponSound(FireFinishSound);
	this->GetOwner()->StopFireAnimation();

	if (MuzzleFXComponent != nullptr)
		MuzzleFXComponent->DeactivateSystem();
	isFiring = false;

	if (triggerReleased)
		GetWorldTimerManager().ClearTimer(WeaponTimer);
}

void AWeapon::SetOwner(AMayhemCharacter* character) { MyOwner = character; }
AMayhemCharacter* AWeapon::GetOwner() { return MyOwner; }

void AWeapon::WeaponTrace() {
	// animation
	this->GetOwner()->StartFireAnimation();

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("weapon tracing"));
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
	static FName MuzzleSocket = FName(TEXT("MuzzleFlashSocket"));
	// Start from the muzzle's position
	FVector StartPos;

	// Get forward vector of owner (from owner's eye)
	FRotator rot;
	this->GetOwner()->GetActorEyesViewPoint(StartPos, rot);
	FVector Forward = rot.Vector();

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
	//	FString::Printf(TEXT("Forward vector: %f, %f, %f "), Forward.X, Forward.Y, Forward.Z));
	// Calculate end position
	//Forward = this->GetActorForwardVector();
	FVector EndPos = (Forward*WeaponRange) + StartPos;
	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	// This fires the ray and checks against all objects w/ collision
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByObjectType(Hit, StartPos, EndPos,
		FCollisionObjectQueryParams::AllObjects, TraceParams);
	// Did this hit anything?
	if (Hit.bBlockingHit)
	{
		Forward.Normalize();
		if (ImpactParticle != nullptr)
			auto emitter = UGameplayStatics::SpawnEmitterAtLocation(this, 
				ImpactParticle, Hit.ImpactPoint);
		//+ Forward * 350);
	}

	float damage = Damage;
	AAI_Character* ai = Cast<AAI_Character>(Hit.GetActor());
	auto x = Hit.GetComponent();
	if (x != nullptr && x->ComponentHasTag(TEXT("HEAD")))
		damage *= 5;

	if (ai) {
		ai->TakeDamage(damage, FDamageEvent(), GetInstigatorController(), this);
	}
}

