// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "CheckpointManager.h"
#include "CheckpointTriggerBox.h"


// Sets default values
ACheckpointManager::ACheckpointManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACheckpointManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACheckpointManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ACheckpointManager::setCheckpoint(ACheckpointTriggerBox* cp)
{
	CurrentCheckPoint = cp;
}

FVector ACheckpointManager::getCurrentSpawnLocation()
{

	return FVector();
}

