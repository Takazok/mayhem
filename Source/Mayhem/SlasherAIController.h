// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "SlasherAIController.generated.h"

/**
 * 
 */
UCLASS()
class MAYHEM_API ASlasherAIController : public AAIController
{
	GENERATED_BODY()

public:
	void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result) override;
private:
	enum state { Start, Chase, Attack, Dead };
	state currState;
	APawn* player;
protected:
	UPROPERTY(EditAnywhere)
		float AttackRange;
};
