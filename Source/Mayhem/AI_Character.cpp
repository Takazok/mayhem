// Fill out your copyright notice in the Description page of Project Settings.

#include "Mayhem.h"
#include "AI_Character.h"
#include "Engine.h"
#include "MayhemProjectile.h"
#include "MayhemCharacter.h"

// Sets default values
AAI_Character::AAI_Character()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//AIControllerClass = AAIController::StaticClass();

}

// Called when the game starts or when spawned
void AAI_Character::BeginPlay()
{
	Super::BeginPlay();

	// give randomness to think delay so they don't think at 
	// the exact same time
	float thinkVariation = thinkDelay * 0.2; // variation
	thinkDelay += FMath::RandRange(-thinkVariation, thinkVariation);

	PlayWalkSound();
}

// Called every frame
void AAI_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAI_Character::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void AAI_Character::DoRangedAttack() {
	auto* player = Cast<AMayhemCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	FRotator rot = (player->GetActorLocation() - GetActorLocation()).Rotation();
	FVector dir = (player->GetActorLocation() - GetActorLocation());
	dir.Normalize();

	AMayhemProjectile* projectile = Cast<AMayhemProjectile>(
		GetWorld()->SpawnActor<AActor>(
			AttackProjectileClass, GetActorLocation(), dir.Rotation()));
	//projectile->GetProjectileMovement()->SetVelocityInLocalSpace(FVector(0, 0, 1));
	//projectile->SetActorRotation(rot.Rotation());

	//AttackProjectileClass, this->GetActorLocation(),
	//this->GetActorRotation());
}

float AAI_Character::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
		EventInstigator, DamageCauser);
	if (ActualDamage > 0.0f)
	{
		Health -= ActualDamage;
		if (Health <= 0.0f)
		{
			// We're dead, don't allow further damage
			bCanBeDamaged = false;
			StopAttack();
			float deathDurration = DeathTimer; //animation time - delay

			FTimerHandle DwarfDeathTimer;

			GetWorldTimerManager().SetTimer
			(
				DwarfDeathTimer,
				this,
				&AAI_Character::DestroyMe,
				deathDurration,
				false,
				deathDurration
			);
			this->GetController()->UnPossess();
		}
	}
	return ActualDamage;
}

void AAI_Character::DestroyMe() {
	// increase player kill count
	APawn* player = UGameplayStatics::GetPlayerPawn(this, 0);
	(Cast<AMayhemCharacter>(player))->IncreaseZombieKills();

	// handle sounds
	PlayDeathSound();
	if (WalkSoundAC != nullptr) {
		WalkSoundAC->Stop();
	}

	// create explosion if the AI is a Fatty
	if (destructionClass != nullptr) {
		auto x = GetWorld()->SpawnActor<AActor>(destructionClass, 
			GetActorLocation(), this->GetActorRotation());
	}
	this->Destroy();
}

void AAI_Character::DamagePlayer() {
	PlayAttackSound();
	APawn* player = UGameplayStatics::GetPlayerPawn(this, 0);
	player->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
}

void AAI_Character::PlayAttackSound() {
	PlaySound(AttackSound);
}

void AAI_Character::PlayDeathSound() {
	PlaySound(DeathSound);
}

void AAI_Character::PlayWalkSound() {
	WalkSoundAC = UGameplayStatics::SpawnSoundAttached(WalkSound,
		RootComponent);
}

void AAI_Character::PlaySound(USoundCue* sound) {
	//GEngine->AddOnScreenDebugMessage(-1, 
	//	15.f, FColor::Red, TEXT("Zombie trying to play sound"));
	if (sound != nullptr) {
		StopSound();
		SoundAC = UGameplayStatics::SpawnSoundAttached(sound,
			RootComponent);
	//	GEngine->AddOnScreenDebugMessage(-1, 
	//		15.f, FColor::Red, TEXT("Zombie playing sound"));
	}
}

void AAI_Character::StopSound() {
	if (SoundAC != nullptr)
		SoundAC->Stop();
	SoundAC = nullptr;
}