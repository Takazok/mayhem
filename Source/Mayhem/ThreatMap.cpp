
#include "Mayhem.h"
#include "ThreatMap.h"
#include "Engine.h"
#include "Cell.h"
#include "Math.h"
#include "AI_Character.h"
#include "AI_Character_Controller.h"
#include "EngineUtils.h"

// Sets default values
AThreatMap::AThreatMap()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AThreatMap::BeginPlay()
{
	Super::BeginPlay();
	FVector2D bounds1(0, 0), bounds2(0, 0);
	GetThreatMapBounds(bounds1, bounds2);

	CreateCells(mResolution, bounds1, bounds2);

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, TEXT("SPAWNING THREATMAP 2"));
	//UpdateAllCellThreats();
}

// Called every frame
void AThreatMap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!UpdateTime(DeltaTime)) {
		return;
	}

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
	//	FString::Printf(TEXT("About to update cells' heuristics")));
	UpdateAllCellThreats();
}

void AThreatMap::UpdateAllCellThreats() {
	for (int i = 0; i < mCells.Num(); i++) {
		for (int j = 0; j < mCells[i].Num(); j++) {
			Cell& cell = mCells[i][j];
			cell.SetThreat(SumOfEnemiesPositionHeuristic(cell));
		}
	}
}

bool AThreatMap::UpdateTime(float deltaTime) {
	mUpdateDelayCount += deltaTime;
	if (mUpdateDelayCount >= mUpdateDelay) {
		mUpdateDelayCount = 0;
		return true;
	}
	return false;
}

void AThreatMap::GetThreatMapBounds(FVector2D& bounds1, FVector2D& bounds2) {
	float xOffset = mBoundsOffset, yOffset = mBoundsOffset;
	//auto player = UGameplayStatics::GetPlayerPawn(this, 0);
	//if (player != nullptr) {
	{
		//FVector origin = player->GetTransform().GetLocation();
		FVector origin = this->GetTransform().GetLocation();
		bounds1.X = origin.X - xOffset;
		bounds1.Y = origin.Y - yOffset;
		bounds2.X = origin.X + xOffset;
		bounds2.Y = origin.Y + yOffset;
	}
}


void AThreatMap::CreateCells(unsigned resolution, FVector2D bounds1, FVector2D bounds2) {
	// get area, cell dimensions
	float area = abs(bounds2.Y - bounds1.Y) * abs(bounds2.X - bounds1.X);
	float cellsPerDim = ceil(sqrt(resolution));
	float cellWidth = (bounds2.X - bounds1.X) / cellsPerDim;
	float cellHeight = (bounds2.Y - bounds1.Y) / cellsPerDim;

	// spawn the cells
	for (int i = 0; i < cellsPerDim; i++) {
		mCells.Add(TArray<Cell>());
		for (int j = 0; j < cellsPerDim; j++) {
			// get cell position
			float x = bounds1.X + cellWidth * j;
			float y = bounds1.Y + cellHeight * i;
			float z = GetTerrainHeight(x, y) + 100; // add some height
			FVector origin(x, y, z);

			// create the cell
			Cell cell = Cell(abs(cellWidth), abs(cellHeight), origin);
			mCells[i].Add(cell);
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue,
			//	FString::Printf(TEXT("Cell position %f, %f, %f"), origin.X, origin.Y, origin.Z));
		}
	}
}

// use a ray cast from (x, y, 5000) to (x, y, -5000) -- the point
// that is hit is probably the the terrain [height], so use that
float AThreatMap::GetTerrainHeight(float x, float y) {
	FVector rayOrigin(x, y, 5000);
	FVector rayEnd(x, y, -5000);

	FName terrainHeightTrace = FName("Terrain Height Trace");
	FCollisionQueryParams traceParams(terrainHeightTrace, true, Instigator);
	FHitResult hit(ForceInit);
	GetWorld()->LineTraceSingleByObjectType(hit,
		rayOrigin, rayEnd, FCollisionObjectQueryParams::AllObjects,
		traceParams);

	// if hit 
	if (hit.bBlockingHit) {
		float z = hit.ImpactPoint.Z;
		return z;
	}
	else {
		return 0; // oh well, can't find it .. 
	}
}

FVector AThreatMap::GetOptimalLocation(AAI_Character_Controller* ai) {
	float lowestThreat = 1000000;
	AAI_Character* aiCharacter = Cast<AAI_Character>(ai->GetPawn());
	float movementDesire = ai->GetMovementDesire();
	float bestFOV = 0;
	bool los = false;
	FVector aiPos = aiCharacter->GetActorLocation();
	FVector bestPosition;
	for (int i = 0; i < mCells.Num(); i++) {
		for (int j = 0; j < mCells[i].Num(); j++) {
			Cell& cell = mCells[i][j];
			//float threat = cell.GetThreat();
			float threat = 1;
			threat += SumOfEnemiesPositionHeuristic(cell, aiCharacter);
			//threat += EnemiesProximityDangerHeuristic(cell, aiCharacter);
			//threat -= EnemiesProximitySafetyHeuristic(cell, aiCharacter);
			// the AI should ignore its own safety and danger field
			//threat += Sigmoid(EnemiesProximitySafetyHeuristic(cell, aiCharacter)
			//	- EnemiesProximityDangerHeuristic(cell, aiCharacter));
			threat += PlayerProximityDangerHeuristic(cell, aiCharacter);
			//threat -= 0.5; // ignore own safety/danger field

			float LOSThreat = PlayerLineOfSightDangerHeuristic(cell, aiCharacter);
			float FOVThreat = 0; 
			if (LOSThreat > 0) {
				// only add FOV threat if the cell is in line of sight
				FOVThreat = PlayerFieldOfViewDangerHeuristic(cell, aiCharacter);
				threat += LOSThreat;
				threat += FOVThreat;
			}

			threat += EnemyKillHeuristic(cell, aiCharacter);
			threat = MovementHeuristic(cell, aiCharacter, threat);
			if (threat < lowestThreat && CanMoveToLocation(cell.GetPosition(), aiCharacter)) {
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
				//	FString::Printf(TEXT(/*"LOS Threat: %f\n*/"FOV Threat: %f"), /*LOSThreat,*/ FOVThreat));
				bestPosition = cell.GetPosition();
				bestFOV = FOVThreat;
				lowestThreat = threat;
				los = LOSThreat > 0.0;
				//PauseMoveToLocation(aiCharacter); // bc 'CanMoveToLocation' makes AI move
			}
		}
	}
	// if the AI has no desire to move .. don't move 
	if (ai->GetMovementDesire() == 0) {
		bestPosition = aiPos;
	}
	/*
	if (los)
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
	FString::Printf(TEXT("in line of sight")));
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
	FString::Printf(TEXT("location: %f, %f, %f"), bestPosition.X, bestPosition.Y, bestPosition.Z));
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
	FString::Printf(TEXT("Threat Level: %f, FOV Level: %f"), lowestThreat, bestFOV));
	*/
	return bestPosition;
}

// the AI should be afraid of areas next to the player
// so if they have a non-zero survival desire, the cells near a player should be more threatening
float AThreatMap::PlayerProximityDangerHeuristic(const Cell& cell, const AAI_Character* ai) {
	auto player = UGameplayStatics::GetPlayerPawn(this, 0);
	float distance = DistanceBetweenCellAndActor(cell, player);
	float survivabilityDesire = ai->GetSurvivabilityDesire();
	float atkRange = ai->GetAttackRange();
	float playerProximityRange = 1000;
	float threat = 0;

	if (ai->GetIsMelee()) {
		// melee AI aren't afraid to move towards the player
		if (distance < atkRange) {
			threat = (atkRange - distance) / atkRange;
			threat = -1 * threat * 0.25;
		}
	}
	else {
		// not melee? get away from the player
		threat = survivabilityDesire * (playerProximityRange - distance) / playerProximityRange;
		threat = (distance > playerProximityRange) ? 0 : threat;
	}
	return Normalize1(threat, 10);
}

// The AI should try to avoid the player's field of vision .. because it's dangerous
float AThreatMap::PlayerFieldOfViewDangerHeuristic(const Cell& cell, const AAI_Character* ai) {
	float FOV = 110; // let's just assume that the player has a FOV of 110
	auto player = UGameplayStatics::GetPlayerPawn(this, 0);
	FVector cellPos = cell.GetPosition();
	FVector playerPos = player->GetTransform().GetLocation();
	FVector vecToPlayer = (cellPos - playerPos);

	FVector2D vecToPlayer2D(vecToPlayer.X, vecToPlayer.Y);
	vecToPlayer2D.Normalize();

	FVector forward = player->GetActorForwardVector();
	FVector2D forward2D(forward.X, forward.Y);
	forward2D.Normalize();

	float angle = abs(360.0 / 2 / 3.14152 * acos(FVector2D::DotProduct(vecToPlayer2D, forward2D)));

	// as the angle between the enemy and the player's forward vec increases, the threat decreases
	// ie: as the angle approaches the player's FOV/2, threat decreases
	// and of course, the enemy only cares about this if they have a desire to survive, so weight the desire
	/*
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
	FString::Printf(TEXT("IN FOV...PLAYER ANGLE: %f"), angle));
	*/
	float threat = 5 * ai->GetSurvivabilityDesire() * ((FOV / 2) - angle) / (FOV / 2);
	threat = (angle > FOV / 2) ? 0 : threat;

	return threat;
}

float AThreatMap::PlayerLineOfSightDangerHeuristic(const Cell& cell, const AAI_Character* ai) {
	FVector rayOrigin = cell.GetPosition();
	auto player = UGameplayStatics::GetPlayerPawn(this, 0);

	// AI controller has functionality for LoS .. so just use that instead of rewriting it
	bool isInSight = (Cast<AAI_Character_Controller>(ai->GetController()))->LineOfSight(rayOrigin);
	if (isInSight) {
		return ai->GetSurvivabilityDesire() * 1;
	}
	else {
		return 0.0;
	}
}

float AThreatMap::SumOfEnemiesPositionHeuristic(const Cell& cell) {
	float threat = 0;
	for (TActorIterator<AAI_Character> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		AAI_Character* ai = *ActorItr;
		float enemySafetyZone = EnemiesProximitySafetyHeuristic(cell, ai);
		float enemyDangerZone = EnemiesProximityDangerHeuristic(cell, ai);

		threat += enemyDangerZone;
		threat -= enemySafetyZone;
	}
	return Sigmoid(threat);
}

float AThreatMap::SumOfEnemiesPositionHeuristic(const Cell& cell, const AAI_Character* aiCharacter) {
	float threat = 0;
	for (TActorIterator<AAI_Character> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		AAI_Character* ai = *ActorItr;
		if (ai != aiCharacter) {
			float enemySafetyZone = EnemiesProximitySafetyHeuristic(cell, ai);
			float enemyDangerZone = EnemiesProximityDangerHeuristic(cell, ai, aiCharacter);

			threat += enemyDangerZone;
			threat -= enemySafetyZone;
		}
	}
	return aiCharacter->GetSpreadDesire() * Sigmoid(threat);
}


// makes a large zone of cells around each enemy safer; this ensures that enemies stay near each other
float AThreatMap::EnemiesProximitySafetyHeuristic(const Cell& cell, const AAI_Character* ai) {
	auto controller = Cast<AAI_Character_Controller>(ai->GetController());
	if (controller == nullptr) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
		//	FString::Printf(TEXT("No controller .. ")));
		return 0;
	}
	float dist = DistanceBetweenCellAndActor(cell, ai);;
	float attackRange = ai->GetAggroRange();
	float threat = (attackRange - dist) / attackRange;

	return ((dist > attackRange) ? 0 : threat);
}

// makes cells directly around each enemy dangerous; this ensures that enemies will 
// spread apartly slighty instead of just forming a clusterf...
float AThreatMap::EnemiesProximityDangerHeuristic(const Cell& cell, const AAI_Character* ai, const AAI_Character* ai2) {
	auto controller = Cast<AAI_Character_Controller>(ai->GetController());
	if (controller == nullptr) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red,
		//	FString::Printf(TEXT("No controller .. ")));
		return 0;
	}
	float dist;
	float spreadDistance;
	float spreadDesire;
	if (ai2 != nullptr) {
		spreadDistance = ai2->GetSpreadDistance();
		spreadDesire = ai2->GetSpreadDesire();
	}
	else {
		spreadDistance = ai->GetSpreadDistance();
		spreadDesire = ai->GetSpreadDesire();
	}
	dist = DistanceBetweenCellAndActor(cell, ai);
	spreadDesire *= 5;
	float threat = spreadDesire * (spreadDistance - dist) / spreadDistance;
	return ((dist > spreadDistance) ? 0 : threat);
}

// if the AI has a low movement desire, then it doesn't want to go move.. IE:
// cells that are farther away have a larger threat
float AThreatMap::MovementHeuristic(const Cell& cell, const AAI_Character* ai, float threat) {
	float distance = DistanceBetweenCellAndActor(cell, ai);
	float attackRange = ai->GetAttackRange();
	float movementDesire = ai->GetMovementDesire();

	distance /= ai->GetAggroRange();
	if (movementDesire == 0 || distance > 1)
		return threat;

	distance = FMath::Pow(distance, 2);
	return threat + (1 - movementDesire) * distance / FMath::Pow(movementDesire, 1.2);
}

/*
The AI obviously wants to kill the player (assuming they have the desire)
so we need a heuristic that allows enemies to position themselves properly.
There are two main AIs: melee and ranged, and thus the heuristics will be different
1. MELEE AI will want to close the distance between the player
If they also want to survive, then they will try to sneak around the player and such
2. Ranged AI will want to remain in LOS & Range of player
If they also want to survive, they will find cover and atk when player is facing away
*/
float AThreatMap::EnemyKillHeuristic(const Cell& cell, const AAI_Character* ai) {
	if (ai->GetIsMelee()) {
		return EnemyKillMeleeHeuristic(cell, ai);
	}
	else {
		return EnemyKillRangedHeuristic(cell, ai);
	}
}

// Heuristics for melee AI: cells that are closer to the player = better
// this assumes that the AI is aggro'd
float AThreatMap::EnemyKillMeleeHeuristic(const Cell& cell, const AAI_Character* ai) {
	APawn* player = UGameplayStatics::GetPlayerPawn(this, 0);
	float distance = DistanceBetweenCellAndActor(cell, ai);
	float distanceFromPlayer = DistanceBetweenCellAndActor(cell, player);
	float aggroRange = ai->GetAggroRange();
	float killDesire = ai->GetKillDesire();

	// the AI should not move towards a cell that is outside its aggro range
	if (distance > aggroRange || distanceFromPlayer > aggroRange)
		return 0;

	// linear threat not sufficient; we want AI to really move in close

	//float threat = 3 * killDesire * (1 - distanceFromPlayer / aggroRange);
	float threat = 4 * killDesire * (1 - FMath::Pow(distanceFromPlayer / aggroRange, 2));
	threat *= -1; // because we want the AI to move towards there

	return threat;
}

// Heuristics for ranged AI: cells that are barely within aggro range & LoS = better
// this assumes that the AI is aggro'd
float AThreatMap::EnemyKillRangedHeuristic(const Cell& cell, const AAI_Character* ai) {
	APawn* player = UGameplayStatics::GetPlayerPawn(this, 0);
	float distance = DistanceBetweenCellAndActor(cell, ai);
	float aggroRange = ai->GetAggroRange();
	float killDesire = ai->GetKillDesire();
	float threat = 0;

	// the AI should not move towards a cell that is outside its aggro range
	if (distance > aggroRange)
		return 0;

	// good cells are located in some area near the edge of the aggro range
	// so imagine a circle around the player, and the edge of the circle has thickness;
	// this thickness is a good area for the AI to move to
	float goodRangePercentage = 0.15; // the thickness of circle's outer edge
	float idealDistance = aggroRange * (1 - goodRangePercentage);
	if (distance < idealDistance || distance > aggroRange) {
		return 0;
	}
	else {
		threat = 3 * (1 - (distance - idealDistance) / (aggroRange - idealDistance));
	}

	return threat;
}
// calculate the distance (only in X and Y coordinates) between cell and an actor (typically an ai)
float AThreatMap::DistanceBetweenCellAndActor(const Cell& cell, const AActor* actor) {
	FVector cellPos = cell.GetPosition();
	FVector actorPos = actor->GetActorLocation();
	float dist = FVector2D::Distance(FVector2D(cellPos.X, cellPos.Y), FVector2D(actorPos.X, actorPos.Y));
	return dist;
}

// calculate the distance (only in X and Y coordinates) between actor and an actor (typically an ai)
float AThreatMap::DistanceBetweenActorAndActor(const AActor* actor, const AActor* actor2) {
	FVector actorPos = actor->GetActorLocation();
	FVector actorPos2 = actor2->GetActorLocation();
	float dist = FVector2D::Distance(FVector2D(actorPos2.X, actorPos2.Y), FVector2D(actorPos.X, actorPos.Y));
	return dist;
}

float AThreatMap::Normalize1(float value, float growth) {
	growth = (growth == 0) ? growth = 0.0001 : 1 / growth;
	return (value) / (value + growth);
}

float AThreatMap::Sigmoid(float value) {
	return 1 / (1 + FMath::Pow(2.71828, -value));
}

float AThreatMap::Root(float value, float rootValue) {
	return FMath::Pow(value, 1.0f / rootValue);
}

bool AThreatMap::CanMoveToLocation(FVector location, AAI_Character* ai) {
	// the result of MovetoLocation is an enum; if it's 0, then the path failed
	// so it is invalid and we cannot move there
	return (Cast<AAI_Character_Controller>(ai->GetController()))->MoveToLocation(location) != 0;
}

void AThreatMap::PauseMoveToLocation(AAI_Character* ai) {
	// when calling "CanMoveToLocation", the AI does actually move
	// so we have to stop it
	Cast<AAI_Character_Controller>(ai->GetController())->StopMovement();
}
